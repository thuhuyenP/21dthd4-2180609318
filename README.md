# 21DTHD4-2180609318
# Phạm Thị Thu Huyền
# 21DTHD4
# 2180609318
# CNTT

| Title | The cashier transfers data to the printer to print tickets |
| --- | --- |
| Value proposition | As a cashier, I want ticket issuance to be easier |
| Acceptance criteria | <ins>Acceptance criteria 5:</ins><br/>Suppose the customer wants to get to the theater more easily <br/> After issuing the ticket for inspection, the customer will be able to enter the theater faster <br/> And ensure that customers will enter the correct theater printed on the ticket|
|Definition of Done | Unit Tests Passed <br> Acceptance Criteria Met <br> Code Reviewed <br> Functional Tests Passed <br> Product Owner Accepts User Story|
|Owner | Mrs.Huyen|
|Interation | |
|Estimate | |


![Casher transfers data to printer to print ticket](https://gitlab.com/thuhuyenP/21dthd4-2180609318/-/raw/main/Casher%20transfers%20data%20to%20printer%20to%20print%20ticket.png?ref_type=heads)


TEST CASE 
| N | Request ID | Test Objective | Test step | Expected Result |
|---|------------|----------------|-----------|-----------------|
| 1 | Req-01     | Print ticket with valid data   | 1. The cashier opens the ticket printing application on the computer. <br>2. The employee enters valid information for the ticket (e.g. ticket type, quantity, ticket price, customer information, date and time).<br>3. The employee selects the ticket printing option. <br>4. The printer responds with a successful print and provides a print ticket. | The ticket is printed successfully and the information on the ticket is correct.|
| 2 | Req-02     | Print ticket with invalid data | 1. The cashier opens the ticket printing application on the computer. <br> 2. The employee entered invalid or blank information (for example, missing customer information). <br>3. The employee selects the ticket printing option. | The system displays an error message or refuses to print the ticket due to invalid data. |
| 3 | Req-03     | Printing tickets with printer error      | 1. The cashier opens the ticket printing application on the computer. <br> 2. The employee enters valid information for the ticket. <br>3. The employee selects the option to print the ticket, but the printer has an error or is not working. | The system notifies the employee that the printer encountered an error or is not working, and logs the error for later processing.|

# Võ Quốc Tân
# 2180608423
# 21DTHD4
# CNTT

## Võ Quốc Tân
| Title | Cashier find movie |
| --- | --- |
| Value Statement | As a cashier, I want to find the right movie according to the customer's request |
| Acceptance Criteria | <ins> Acceptance Criteria 2: </ins><br/>Movies are selected based on customer requests.<br/>After that, show the customer's selected movie information.</br> The customer will check the movie information again.
|Definition of Done | Unit Tests Passed <br> Acceptance Criteria Met <br> Code Reviewed <br> Functional Tests Passed <br> Product Owner Accepts User Story|
|Owner | Mr. Tan |
|Interation | |
|Estimate | |
|UI|![Cashier find movie](https://gitlab.com/quoctan1/21dthd4-2180608423/-/raw/main/PictureFind.png?ref_type=heads)

# 21DTHD4-2180608710
## Nguyễn Nhựt Minh 
## Lop: 21DTHD4
## MSSV: 2180608710
## Khoa: CNTT

|Title:               | Cashier Logs in|
|:-------------------:|-----------------------------------------------------------------------------------------------------------------------------------------:|
|Values Statement     | As a cashier, I want when I log in, it will take me to the page I work on, so I can grasp my work.                                                    |
|Acceptance Criteria  | Acceptance Criteria: When I logged into the software with my employee ID the system confirmed I was the cashier and entered my workplace.|
|Definition of Done | Unit Tests Passed <br> Acceptance Criteria Met <br> Code Reviewed <br> Functional Tests Passed <br> Product Owner Accepts User Story|
|Owner | Mr Minh|
|Interation | |
|Estimate | |
|UI| ![Cashier_Login](https://gitlab.com/nhutminhnguyen/21dthd4-2180608710/-/raw/main/Cashier_Login.png?ref_type=heads)|

# 21DTHD4-2180607948
# Nguyễn Hữu Quý 
# Lớp: 21DTHD4
# MSSV: 2180607948
# Khoa: CNTT

## Nguyễn Hữu Quý 
| Title | Cashier confirms customer has paid |
| --- | --- |
| Value Statement | As an audience, I want to watch an interesting movie |
| Acceptance Criteria | <ins>Acceptance Criteria 4:</ins><br/>Given that the customer chose the seats they had ordered <br/> Then the staff will confirm the payment method which the customer has chosen</br> And ensure that the customers have tranfered money or paid by cash |
|Definition of Done | Unit Tests Passed <br> Acceptance Criteria Met <br> Code Reviewed <br> Functional Tests Passed <br> Product Owner Accepts User Story|
|Owner | Mr. Quý|
|Interation | |
|Estimate | |
|UI|![Cashier confirms customer has paid](https://gitlab.com/no-name9340436/21dthd4-2180607948/-/raw/main/Pay.png?ref_type=heads)|


# 21DTHD4-2180608451
## Trương Anh Tuấn 
## Lớp : 21DTHD4
## MSSV:2180608451
## Khoa : CNTT

| title                 | Cashier views selected seats   | 
| -------------         |:-------------:| 
| Value statement       |   As a Cinema Customer :  I want to watch good movies|
| Acceptance Criteria   |   Acceptance Criteria :When customers choose,  Cashier must ensure that the seats they choose are empty, otherwise they must notify the customer so they can have another choice.|
|Definition of Done     | Unit Tests Passed <br> Acceptance Criteria Met <br> Code Reviewed <br> Functional Tests Passed <br> Product Owner Accepts User Story|
|Owner                  | Mr.Tuan|

![Cashier views selected seats](https://gitlab.com/TruongAnhTuan/21dthd4-2180608451/-/raw/main/Cashier%20views%20selected%20seats.png?ref_type=heads)

